import { Component, OnInit } from '@angular/core';
import { StocksService } from '../stocks.service';
import { MatTableDataSource } from '@angular/material';

interface Stocks {
    id: number,
    name: string,
    price: string,
    category:string,
    quantity: number
}

interface userBalance {
    userBalance: number
}

@Component({
  selector: 'app-markets',
  templateUrl: './markets.component.html',
  styleUrls: ['./markets.component.css']
})
export class MarketsComponent implements OnInit {
  markets: Stocks[] = [];
  dataSource;
  
  buyAmount: number = 0;
  
  oldBalance: number;
  
  displayedColumns: string[] = ['name', 'price', 'quantity', 'Buy_Button'];
    
  categoriesSet = new Set();

  constructor(private stocksService: StocksService) { }

  ngOnInit() {
     this.stocksService.getMarkets().subscribe((stocks: Stocks[])=>{
         this.markets = stocks;
         for (let market of this.markets) {
          market.quantity = null;
          this.categoriesSet.add(market.category);
          }
         this.categoriesSet.add('');
         this.dataSource =  new MatTableDataSource(this.markets);
      }); 
    
  
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
}; 
  

  buyStocks(row) {
     this.stocksService.changeBalanceBuy(row);
     this.stocksService.addToProfile(row).subscribe((json)=>{});
     this.stocksService.getUserBalance().subscribe((balance: userBalance)=>{
        this.oldBalance = balance.userBalance;       this.stocksService.setUserBalanceBuy(this.oldBalance).subscribe((json)=>{})
    });
     
     row.quantity = null;
  }

}
