import { Component, OnInit } from '@angular/core';
import { StocksService } from '../stocks.service';

interface portfolioStocks {
    id: number,
    name: string,
    price: string,
    category:string,
    quantity: number,
    sell_quantity:number
}

interface userBalance {
    userBalance: number
}

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
    markets: portfolioStocks[] = [];
    
    totalValue: number = 0;
    sellValue: number;
    oldBalance:number;

  constructor(private stocksService: StocksService) { }

  ngOnInit() {
    this.stocksService.getPortfolioMarkets().subscribe((stocks: portfolioStocks[])=>{
         this.markets = stocks;
         for (let market of this.markets) {
          market.sell_quantity = null;
          }
          for (let market of this.markets) {
        let stock_value = +market.price * market.quantity;
        this.totalValue += stock_value;
        
    }
    });
  }
  
    displayedColumns: string[] = ['name', 'price', 'quantity',  'sell_quantity', 'Buy_Button'];
  
  sellStocks(row){
      if (row.sell_quantity > row.quantity) {
          alert('you want to sell more stocks than you have')
      }else {
          row.quantity -=row.sell_quantity;
          this.sellValue = +row.price * row.sell_quantity;
          this.totalValue -= this.sellValue;
          row.sell_quantity = null;
          if (row.quantity > 0) {                this.stocksService.changeProfileRow(row).subscribe((json)=>{});
          } else { 
              this.stocksService.deleteProfileRow(row).subscribe((json)=>{
                  this.markets = this.markets.filter( m => m.id !== row.id);
              })
          };
          
          this.stocksService.getUserBalance().subscribe((balance: userBalance)=>{
            this.oldBalance = balance.userBalance;
            let newBalance = this.oldBalance + this.sellValue; this.stocksService.setUserBalanceSell(newBalance).subscribe((json)=>{});
          });
          
      }
  }

}
