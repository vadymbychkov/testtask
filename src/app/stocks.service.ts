import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()

export class StocksService {

    constructor(private http: Http){}
    
    buyValue: number; 
    
    
    getUserBalance(){
        return this.http.get('http://localhost:3000/user').map((response: Response)=> response.json()
        );
    }
    
    setUserBalanceBuy(userBalance: number) {
         let balance = {
            "userBalance": userBalance - this.buyValue
         } 
         return this.http.put('http://localhost:3000/user', balance).map((response: Response)=> response.json());
    } 
    
    setUserBalanceSell(userBalance: number) {
         let balance = {
            "userBalance": userBalance
         } 
         return this.http.put('http://localhost:3000/user', balance).map((response: Response)=> response.json());
    }
    
     
    getMarkets(){
        return this.http.get('http://localhost:3000/markets').map((response: Response)=> response.json()
        );
    }    
    getPortfolioMarkets(){
        return this.http.get('http://localhost:3000/portfolio').map((response: Response)=> response.json()
        );
    }
    
    changeBalanceBuy(row){
        this.buyValue = +row.price * row.quantity;        
        
    }
    
    addToProfile(row){
        const portfolioRow = {
         name: row.name,
         price: row.price,
         category: row.category,
         quantity: row.quantity
        }
        return this.http.post('http://localhost:3000/portfolio', portfolioRow).map((response: Response)=> response.json());
    }
    
    changeProfileRow(row) {
         return this.http.put(`http://localhost:3000/portfolio/${row.id}`, row).map((response: Response)=> response.json());
    }
    
    deleteProfileRow(row) {
        return this.http.delete(`http://localhost:3000/portfolio/${row.id}`).map((response: Response)=> response.json());
    }
    
}