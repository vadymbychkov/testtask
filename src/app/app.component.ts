import { Component, OnInit } from '@angular/core';
import { StocksService } from './stocks.service';

interface userBalance {
    userBalance: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'StockTraderApp';
  
  userBalance: number;
  
  constructor(private stocksService: StocksService) { }
  
  ngOnInit(){
    this.stocksService.getUserBalance().subscribe((balance: userBalance)=>{
        this.userBalance = balance.userBalance;
      });
      
  }  

}
