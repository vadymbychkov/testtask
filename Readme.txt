To run application on local please follow the next steps:

1. Download the folder with the application.
2. Open two command prompts(cmd), on both open the folder with the application.
3. On first cmd window type and run  following: "npm install" - to inslall folder node_modules
4. After node_modules folder installed, on first cmd window type and run  following "json-server --watch stocks.json" - to run the server
5. On the second cmd window type and run  following "ng serve" - to run application